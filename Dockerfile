# Dockerfile use for the ci
FROM node:12-alpine

ARG NODE_ENV=development
ARG NPM_CONFIG_LOGLEVEL=${NPM_CONFIG_LOGLEVEL:-warn}

# Install build toolchain, install node deps and compile native add-ons
# RUN apk add --no-cache --virtual .gyp python make g++

RUN mkdir -p /usr/src/app/ && chown node:node /usr/src/app/
WORKDIR /usr/src/app/

## install dependencies
USER node
COPY package*.json ./
RUN npm ci && npm cache clean --force

COPY . .

CMD ["echo", "world"]

