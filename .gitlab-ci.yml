---
variables:
  GIT_STRATEGY: clone
  GSG_RELEASE_BRANCHES: "master"
  RELEASE_DESC: "Configuration Management"

stages:
  - semver
  - prepare
  - build
  - test
  - publish
  - release

include:
  - template: Code-Quality.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml

#
# [semver stage]
#
# Create:
# - ".next-version" file with the next version bump to be commited
# - "CHANGELOG" file if needed
#

semver:
  stage: semver
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  script:
  - release test-git || true
  - release test-api
  - release next-version --bump-patch > .next-version
  - release changelog || true
  - cat .next-version
  artifacts:
    paths:
    - .next-version
    - CHANGELOG.md
    expire_in: 1d
  except:
    - tags
    - schedules

#
# [prepare stage]
#
# Update package*.json files
#

prepare:
  image: node:lts-buster
  stage: prepare
  script:
    - npm --version
    - npm version --no-git-tag-version $(cat .next-version)
  artifacts:
    paths:
    - package.json
    - package-lock.json
  except:
    - tags
    - schedules

#
# [build stage]
#
# Build node module and saved them in cache
#

build:
  stage: build
  image: node:lts-buster
  script:
    - npm ci
  cache: &global_cache
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
    policy: pull-push
  except:
    - tags
    - schedules

#
# [test stage]
#
# Everything related to code quality can be put in this stage
# e.g.
# - security
# - lint
# - test
#

code_quality:
  artifacts:
    expire_in: 6 mos
    reports:
      codequality: gl-code-quality-report.json

license_scanning:
  artifacts:
    expire_in: 6 mos
    reports:
      license_scanning: gl-license-scanning-report.json

test:js:
  image: node:lts-buster
  stage: test
  dependencies:
    - build
  script:
    - npm test
  cache:
    <<: *global_cache
    policy: pull
  except:
    - tags
    - schedules
  artifacts:
    reports:
      junit: junit.xml
  coverage: /All files\s*\|\s*([\d\.]+)/

lint:js:
  image: node:lts-buster
  stage: test
  dependencies:
    - build
  script:
    - npm run lint
  cache:
    <<: *global_cache
    policy: pull
  except:
    - tags
    - schedules

#
# [publish stage]
#
# Publish package on npm
#

publish:
  image: node:lts-buster
  stage: publish
  when: manual
  allow_failure: false
  dependencies:
    - prepare
  script:
    - echo "//registry.npmjs.org/:_authToken=$NPM_TOKEN" > .npmrc
    - npm publish
  only:
    - master
  except:
    - tags
    - schedules

#
# [release stage]
#
# branches:
# - nothing just exist with success and go to the next stage
#
# master
# - create new release notes
# - commit changelog, release note
# - tagged the branch
#

.release_info_definition: &release_info_definition
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  stage: release
  when: on_success

release:
  <<: *release_info_definition
  dependencies:
  - semver
  - prepare
  - publish
  script: |
    release test-git && {
      echo "[YES] some changes have been detected."
      release commit-and-tag CHANGELOG.md package.json package-lock.json
      release --ci-commit-tag $(cat .next-version)
    } || {
      echo "[NO] no changes have been detected."
    }
  only:
    - master
  except:
    - schedules
    - tags
