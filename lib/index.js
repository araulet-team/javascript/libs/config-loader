const nconf = require('nconf')
const debug = require('debug')('libs:configloader')
const fs = require('fs')

let nconfDefefaultOpts = {
  envs: [],
  overrides: {},
  defaults: {}
}

/**
 * Load application configuration
 *
 * @param {String} pathToConfig   configuration file path
 * @param  {Object} nconfOpts     nconf options (optional)
 *
 * @return {Object} nconf configuration
 */
function loadConfig (pathToConfig, nconfOpts) {
  debug('[start] loading/preparing global configuration.')

  const contents = _loadConfiguration(pathToConfig)
  const opts = _mergeNconfOpts(nconfOpts)

  // 1. any overrides
  nconf.overrides(opts.overrides)

  // setup nconf to use (in-order):
  //   2. command-line arguments
  //   3. environment variables
  nconf.argv()
    .env(opts.envs)
    .required(['NODE_ENV'])

  // 4. load application configuration
  contents.forEach((content, index) => {
    nconf.add(`app${index}`, { type: 'literal', store: content })
  })

  // 5. any default values
  nconf.defaults(opts.defaults)

  debug('[end] loading global configuration.')

  return nconf
}

/**
 * load application configuration depending of NODE_ENV
 *
 * @param  {String} pathToConfig    configuration folder
 *
 * @return {Object}                 configuration object
 */
function _loadConfiguration (pathToConfig) {
  if (!pathToConfig) {
    throw new Error(`something went wrong: no configuration folder passed as argument ${pathToConfig}`)
  }

  debug(`[start] load all configuration files from folder: ${pathToConfig}`)
  const files = getFilesFromDir(pathToConfig)
  const contents = getFilesContent(files)
  debug(`[end] load all configuration files contents: ${JSON.stringify(contents, null, 2)}`)

  return contents
}

/**
 * Return all file name from a specific folder
 *
 * @param  {String} dir   directory to be read
 *
 * @return {Array}        all files that match the current env
 */
function getFilesFromDir (dir) {
  let files

  try {
    files = fs.readdirSync(dir)
  } catch (e) {
    throw Error(`no directory found ${dir}`)
  }

  files = filterFilesByEnv(dir, files)
  debug('files to be load by the application: ', files)

  return files
}

/**
 * Retrun filtering array of file name for a specific environment
 *
 * @param  {String} dir   directory to be read
 * @param  {Array} files  all file name to be loaded to be filtered
 *
 * @return {Array}        array with filtering files path
 */
function filterFilesByEnv (dir, files) {
  debug(`process.env.NODE_ENV: ${process.env.NODE_ENV}`)

  return files.filter(file => (file.indexOf(process.env.NODE_ENV) > -1))
    .map(fileName => `${dir}/${fileName}`)
}

/**
 * Return an array of object containing all configuration
 *
 * @param  {Array} files   all file name to be loaded
 *
 * @return {Array}
 */
function getFilesContent (files) {
  return files.map((file) => {
    const content = fs.readFileSync(file)

    return JSON.parse(content)
  })
}

/**
 * Merge default missing options with the ones provided
 * @param  {Object} opts
 *
 * @return {Object}       Object with all properties needed for the libs
 */
function _mergeNconfOpts (opts) {
  const finalOpts = Object.assign({}, nconfDefefaultOpts, opts)

  return finalOpts
}

module.exports = loadConfig
