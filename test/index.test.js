const path = require('path')

const loadConfiguration = require('../lib/index.js')

describe('#index', () => {
  let configFolder

  beforeEach(() => {
    configFolder = path.join(__dirname, 'fixture')
  })

  describe('#success', () => {
    it('should load default configuration', () => {
      const opts = {
        overrides: { name: 'arno1' },
        defaults: { name: 'arno2' },
        envs: ['NODE_ENV']
      }

      const result = loadConfiguration(configFolder, opts)
      expect(result.get('name')).toEqual('arno1')
    })

    it('should load default configuration for prod environments', () => {
      const opts = {
        overrides: { name: 'arno1' },
        defaults: { name: 'arno2' },
        envs: ['NODE_ENV']
      }
      process.env.NODE_ENV = 'production'

      const result = loadConfiguration(configFolder, opts)
      expect(result.get('env')).toEqual('prod')
    })

    it('should load default configuration for test environments', () => {
      const opts = {
        overrides: { name: 'arno1' },
        defaults: { name: 'arno2' },
        envs: ['NODE_ENV']
      }
      process.env.NODE_ENV = 'test'

      const result = loadConfiguration(configFolder, opts)
      expect(result.get('env')).toEqual('test')
    })
  })
})
