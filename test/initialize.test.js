const path = require('path')

const loadConfiguration = require('../lib/index.js')

// test that params are well handled

describe('#index', () => {
  let configFolder

  beforeEach(() => {
    configFolder = path.join(__dirname, 'fixture')
  })

  describe('#success', () => {
    it('should success when args are passed', () => {
      const result = loadConfiguration(configFolder)
      expect(result).toBeInstanceOf(Object)
    })

    it('should merge default nconf options', () => {
      const result = loadConfiguration(configFolder)
      expect(result).toBeInstanceOf(Object)
    })

    it('should merge default nconf options with the ones provided', () => {
      const result = loadConfiguration(configFolder, { envs: ['NODE_ENV'] })
      expect(result).toBeInstanceOf(Object)
    })
  })

  describe('#failure', () => {
    it('should failed if config folder is not passed', () => {
      expect(() => {
        loadConfiguration()
      }).toThrowError(/something went wrong/)
    })

    it('should failed if config folder is not actually a folder', () => {
      expect(() => {
        loadConfiguration('notafolder')
      }).toThrowError(/no directory found/)
    })
  })
})
