# Simple nconf loader [![pipeline status](https://gitlab.com/araulet-team/javascript/libs/template/badges/master/pipeline.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) [![coverage report](https://gitlab.com/araulet-team/javascript/libs/template/badges/master/coverage.svg)](https://gitlab.com/araulet-team/javascript/libs/template/commits/master) 

Simple wrapper around [nconf](https://github.com/indexzero/nconf)

    Hierarchical node.js configuration with files, environment variables, command-line arguments, and atomic object merging.

## Installation

```shell
$ npm install --save simple-nconf-loader
```

## Usage

#### Initializing the module

```
const loadConfiguration = require('simple-nconf-loader')
const configFolder = path.join(__dirname, 'config')
const nconfOpts = {
  envs: [],
  overrides: {},
  defaults: {}
}

const config = loadConfiguration(configFolder, nconfOpts)

# later config can be call like this
config.get('key')
```

This module takes 2 arguments
* **configuration folder**: absolute path to the configuration folder
* **nconf options**: object containing the following keys: envs, overrides, defaults (go to [nconf documentation](https://github.com/indexzero/nconf))

#### Loading configuration file

To load the good configuration files, the module relies on `NODE_ENV`.

If configuration folder contains
* development.tpl.json
* production.tpl.json
* test.tpl.json

```shell
NODE_ENV=production node index.js
```

This will load and merge `production.tpl.json`.

## Test

* `npm test`
* `npm run lint`: standard
* `npm run lint:fix`: standard

## Authors

* **Arnaud Raulet**

## License

MIT License

Copyright (c) 2018 Arnaud Raulet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
