# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 1.2.1
2020-06-14

### Fixes

- **ci:** jest reporters (98323a31990143eb78598b3d4cff5527df4c7a3f)

## 1.2.0
2020-06-14

### Features

- **ci:** trigger dependency scanning on schedules[skip-ci] (01983a0c96b658b55ff633f3fffd5ebdecfe7851)
- update gitlab-ci.yml (f396d04bb23c0b8349bec5989ec168a732e9a1fe)

